

```python
%matplotlib inline
```


Autograd: 自动微分
===================================

PyTorch中所有神经网络包中最核心的是``autograd``包。让我们先简要的看下``autograd``包，然后使用该包来训练我们的第一个神经网络。

``autograd``包Torch下关于张量自动微分的所有运算操作。它是一个动态框架，使用该动态框架你可以通过代码来自定义梯度反向传播的过程，并且是的每一次迭代过程均可以不同。

让我们看一些例子来学习一些简单的模式。

张量
--------

``torch.Tensor``是Torch包的核心类。如果设置其属性``.requires_grad``为``True``，那么该张量则可以跟踪所有的微分操作。当你完成了你的计算后，可以调用``.backward()``用来自动计算所有的梯度。该张量的梯度运算结果将会存储到``.grad``属性中。

如果想停止自动微分等操作，可以调用``.detach()``来解除自动微分，并且后面的计算也不会再自动微分。

另外一种解除自动微分的方法是，是使用代码块``with torch.no_grad():``。该代码在评估一个模型是非常有用的，因为模型一般是通过设置``requires_grad=True``来训练参数，但是评估时我们不需要这些梯度值。

还有一个对自动梯度实现很重要的类-``Function``。

``Tensor``和``Function``是相互关联的，并且二者通过一个循环计算图构建在一起的，进而实现一个完整的计算历史。每个张量tensor拥有一个``.grad_fn``属性来表示创建``Tensor``的函数``Function``。（除非一个Tensor是被用户创建的，这种情况下有个``grad_fn is None``）

如果你想计算微分，可以调用在对应的``Tensor``上调用``.backward()``。如果``Tensor``是标量（也就是零维张量，这个张量只有一个元素），你不需要给张量的函数``backward``提供任何变量，仅仅需要与待计算张量尺寸一致的``gradient``变量。


```python
import torch
```

创建一个张量，并设置``requires_grad=True``来跟踪该张量的所有运算。


```python
x = torch.ones(2, 2, requires_grad=True)
print(x)
```

    tensor([[1., 1.],
            [1., 1.]], requires_grad=True)


进行操作（加法，类似numpy的广播运算）：


```python
y = x + 2
print(y)
```

    tensor([[3., 3.],
            [3., 3.]], grad_fn=<AddBackward0>)


``y``是通过运算得到的，因此``y``拥有属性``grad_fn``


```python
print(y.grad_fn)
```

    <AddBackward0 object at 0x7fdab4f9d5f8>


在运算得到的张量``y``上进行更多的运算操作。


```python
z = y * y * 3
out = z.mean()

print(z, out)
```

    tensor([[27., 27.],
            [27., 27.]], grad_fn=<MulBackward0>) tensor(27., grad_fn=<MeanBackward1>)


``.requires_grad_(...)``内建函数会改变现有张量的``requires_grad``属性。``.requires_grad_()``默认输入参数是``False``


```python
a = torch.randn(2, 2)
a = ((a * 3) / (a - 1))
print(a.requires_grad)
a.requires_grad_(True)
print(a.requires_grad)
b = (a * a).sum()
print(b.grad_fn)
```

    False
    True
    <SumBackward0 object at 0x7fdad240a240>


1.2 梯度
---------
开始反向传播。

因为``out``张量中包含了单个的标量，所以``out.backward()``等价于``out.backward(torch.tensor(1,))``。


```python
out.backward()
```

```python
print(x.grad)
```

    tensor([[4.5000, 4.5000],
            [4.5000, 4.5000]])

上面的cell中的print(x.grad)得到一个元素为4.5的矩阵。为描述方便，这里令张量"$o$"表示变量``out``。我们有
$$
o = \frac{1}{4} \sum_i z_i
$$
其中$z_i = 3 \left( x_i + 2\right) ^2​$和$z_i\mid_{x_i=1}=27​$。

因此有
$$
\frac{\partial o}{\partial x_i} = \frac{3}{2}(x_i + 2)
$$
于是有
$$
\frac{\partial o}{\partial x_i} \mid_{x_i=1} = \frac{9}{2} = 4.5
$$
从数学上而言，如果向量函数映射关系为$\vec{y}=f(\vec{x})​$，那么向量$\vec{y}​$对向量$\vec{x}​$的梯度是一个Jacobian矩阵：
$$
\begin{align}J=\left(\begin{array}{ccc}
   \frac{\partial y_{1}}{\partial x_{1}} & \cdots & \frac{\partial y_{1}}{\partial x_{n}}\\
   \vdots & \ddots & \vdots\\
   \frac{\partial y_{m}}{\partial x_{1}} & \cdots & \frac{\partial y_{m}}{\partial x_{n}}
   \end{array}\right)\end{align}
$$
一般而言，``torch.autograd``是一个计算向量-Jacobian乘积的引擎。给定$v=\left(\begin{array}{cccc} v_{1} & v_{2} & \cdots & v_{m}\end{array}\right)^{T}​$，计算乘积$v^{T}\cdot J​$。如果$v​$是标量函数$l=g\left(\vec{y}\right)​$的梯度值，于是$v=\left(\begin{array}{ccc}\frac{\partial l}{\partial y_{1}} & \cdots & \frac{\partial l}{\partial y_{m}}\end{array}\right)^{T}​$，通过链式法则，向量Jacobian乘积可以认为是$l​$对向量$\vec{x}​$取的梯度。
$$
\begin{align}J^{T}\cdot v=\left(\begin{array}{ccc}
   \frac{\partial y_{1}}{\partial x_{1}} & \cdots & \frac{\partial y_{m}}{\partial x_{1}}\\
   \vdots & \ddots & \vdots\\
   \frac{\partial y_{1}}{\partial x_{n}} & \cdots & \frac{\partial y_{m}}{\partial x_{n}}
   \end{array}\right)\left(\begin{array}{c}
   \frac{\partial l}{\partial y_{1}}\\
   \vdots\\
   \frac{\partial l}{\partial y_{m}}
   \end{array}\right)=\left(\begin{array}{c}
   \frac{\partial l}{\partial x_{1}}\\
   \vdots\\
   \frac{\partial l}{\partial x_{n}}
   \end{array}\right)\end{align}​
$$
（注意到$v^{T}\cdot J​$给出一个行向量，这个行向量与$J^{T}\cdot v​$这个列向量的元素一致，二者为转置关系）

向量-Jacobian乘积的特性，使得非标量输出的模型进行梯度计算非常方便。

现在我们举例来计算向量-Jacobian乘积运算。


```python
x = torch.randn(3, requires_grad=True)

y = x * 2
while y.data.norm() < 1000:
    y = y * 2

print(y)
```

    tensor([ 1193.3157, -1552.5461,    48.4132], grad_fn=<MulBackward0>)


上面cell中的``y``不再是一个标量。``torch.autograd``是不能直接计算完全Jacobian矩阵的，但是如果我们想计算向量-Jacobian乘积，只需要传递一个向量到backward参数中即可。


```python
v = torch.tensor([0.1, 1.0, 0.0001], dtype=torch.float)
y.backward(v)

print(x.grad)
```

    tensor([1.0240e+02, 1.0240e+03, 1.0240e-01])


与1.1节介绍一样，可以使用``with troch.no_grad():``来解除属性``.requires_grad=True``设定的梯度跟踪设置。


```python
print(x.requires_grad)
print((x ** 2).requires_grad)

with torch.no_grad():
	print((x ** 2).requires_grad)
```

    True
    True
    False


**进一步阅读:**

更多的关于``autograd``和``Function``的文档可以找这里
https://pytorch.org/docs/autograd


